package clase.jsonparse;

import java.io.Serializable;

public class Product implements Serializable{


    public int idProduct;
    public String title;
    public String shortDescription;
    public String price;
    public String available;
    public String description;
    public int idCategory;
    public String image;



    public Product(int idProduct, String title, String shortDescription, String price, String available, String description, int idCategory, String image) {
        this.idProduct = idProduct;
        this.title = title;
        this.shortDescription = shortDescription;
        this.price = price;
        this.available = available;
        this.description = description;
        this.idCategory = idCategory;
        this.image = image;


    }


}
